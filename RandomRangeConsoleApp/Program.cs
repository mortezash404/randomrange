﻿using System;
using System.Linq;

namespace RandomRangeConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var random = new Random();

            var enumerable = Enumerable.Range(1, 10);

            foreach (var i in enumerable)
            {
                Console.WriteLine(random.Next(20));
            }
        }
    }
}
